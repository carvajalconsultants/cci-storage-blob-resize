export functionapp="CRFlexyMaxQCPhotoResize"
export resourceGroupName="RG01"

az functionapp deployment source config --name $functionapp \
  --resource-group $resourceGroupName --branch master --manual-integration \
  --repo-url https://miguelocarvajal@bitbucket.org/carvajalconsultants/cci-storage-blob-resize.git
